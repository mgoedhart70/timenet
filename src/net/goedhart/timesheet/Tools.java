package net.goedhart.timesheet;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Tools {

	private static SimpleDateFormat timestamp_format = 
			new SimpleDateFormat("yyyyMMddHHmmssSSS");

	public static String timestamp() {
		timestamp_format.setTimeZone(TimeZone.getTimeZone("CEST"));
		return timestamp_format.format(new Date());
	}
}
