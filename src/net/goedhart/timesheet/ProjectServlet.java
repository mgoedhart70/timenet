/**
 * Copyright 2011 Google
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.goedhart.timesheet;

import java.io.*;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.goedhart.timesheet.model.Project;
import net.goedhart.timesheet.model.ProjectSerializer;

import org.apache.log4j.Logger;

import com.google.appengine.api.NamespaceManager;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * This servlet responds to the request corresponding to project entities. The servlet
 * manages the project Entity
 * 
 * 
 */
@SuppressWarnings("serial")
public class ProjectServlet extends BaseServlet {

    private static final Logger logger = Logger.getLogger(ProjectServlet.class);
    
	private SimpleDateFormat date_format = new SimpleDateFormat("dd-MM-yyyy");
	private Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy").create();
	
	private enum Operation { add, edit, del };

	protected void doList(HttpServletRequest req, HttpServletResponse resp, User user)
			throws ServletException, IOException {
		
		logger.debug("Obtaining project listing");
		
		logger.debug("sord=" + req.getParameter("sord"));
		logger.debug("page=" + req.getParameter("page"));
		logger.debug("sidx=" + req.getParameter("sidx"));
		logger.debug("_search=" + req.getParameter("_search"));
		logger.debug("rows=" + req.getParameter("rows"));
		
		int startIndex = 0;
		int totalRecords = 0;
		int page = Integer.parseInt(req.getParameter("page"));
		int pageSize = Integer.parseInt(req.getParameter("rows"));
		String sortfield =  req.getParameter("sidx");
		
		boolean ascending = false;
		if (req.getParameter("sord").equalsIgnoreCase("asc")) ascending = true;
			
		logger.debug("sort=" + sortfield + ascending);
		
		
		PrintWriter out = resp.getWriter();
		
		startIndex = pageSize * page - pageSize;
		List<Project> list = Project.list(startIndex, pageSize, sortfield, ascending);

		Gson gsonb = new GsonBuilder()
			.setDateFormat("yyyy-MM-dd hh:mm:ss")
			.registerTypeAdapter(Project.class, new ProjectSerializer())
			.create();
  		Map<String, Object> record = new HashMap<String, Object>();
  		
  		totalRecords = Project.size();
  		record.put("currentpage", page);
  		record.put("totalpages", totalRecords / pageSize + 1);
		record.put("totalrecords", totalRecords);

		record.put("projects", list);

		logger.debug("result:" + gsonb.toJson(record));
		
		out.println(gsonb.toJson(record));
	}

	/**
	 * Create the project entity
	 */
	protected void doCreate(Project proj, HttpServletResponse resp, User user)
			throws ServletException, IOException {
		
		logger.debug("Creating project");
		PrintWriter out = resp.getWriter();
		String result = "error";
		
		try {
			
			proj.create(user);
			
			result = "success";
				
		} catch (Exception e) {

			result = e.getMessage();
			logger.error(result);
		}

		if (result.equals("success"))
			out.println(result);
		else
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, result);

	}

	/**
	 * Update the project entity
	 */
	protected void doUpdate(Project proj, HttpServletResponse resp, User user)
			throws ServletException, IOException {
		
		logger.debug("Updating project");

		PrintWriter out = resp.getWriter();
		String result = "";
		
		try {
			
			proj.update(user);

			result = "success";
			
		} catch(Exception e) {
			
			result = e.getMessage();
			logger.error(result);

		}
			
		if (result.equals("success"))
			out.println(result);
		else
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, result);
		
	}

	/**
	 * Delete the project entity
	 */
	protected void doDelete(Project proj, HttpServletResponse resp, User user)
			throws ServletException, IOException {
		
		PrintWriter out = resp.getWriter();
		String result = "";
		
		try {
			proj.delete();
			
			result = "success";
			
		} catch(Exception e) {
			result = e.getMessage();
			logger.error(result);
		}
		
		if (result.equals("success"))
			out.println(result);
		else
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, result);
	}

	/**
	 * Redirect the call to doDelete or doPut method
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		super.doPost(req, resp);
		
		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();
		
		logger.debug("domain=" + user.getAuthDomain());

		logger.debug("namespace="+NamespaceManager.get());
		
		String operation = req.getParameter("oper");

		// put post parameters in map for json 
		Map<String, Object> record = new HashMap<String, Object>();
		
		@SuppressWarnings("unchecked")
		Enumeration<String> enume = req.getParameterNames();
		while (enume.hasMoreElements()) {
			String s = enume.nextElement();
			
			// when add operation customer_id equals _empty
			if (s.equalsIgnoreCase("project_id")) {
				try {
					long id = Long.valueOf(req.getParameter(s));
					record.put(s, id);
				} catch (NumberFormatException e) {}
			} else
				record.put(s, req.getParameter(s));
		}

		// put empty date as null in project
		GsonBuilder gsonBuilder = new GsonBuilder();
	    gsonBuilder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

	        @Override
	        public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
	                throws JsonParseException {
	            try {
	                return date_format.parse(json.getAsString());
	            } catch (ParseException e) {
	                return null;
	            }
	        }
	    });
	    Gson gson = gsonBuilder.create();
	    
		String querystring = gson.toJson(record);
		logger.debug("querystring=" + querystring);
		
		if (operation == null) operation = "list";
		
		Project proj = null;
		try {
			Operation.valueOf(operation);
			proj = gson.fromJson(gson.toJson(record), Project.class);
		} catch (Exception e) {}

		if (operation.equalsIgnoreCase("del")) {
			doDelete(proj, resp, user);
			return;
		} else if (operation.equalsIgnoreCase("add")) {
			doCreate(proj, resp, user);
			return;
		} else if (operation.equalsIgnoreCase("edit")) {
			doUpdate(proj, resp, user);
			return;
		} else {
			doList(req, resp, user);
		}
	}

}

