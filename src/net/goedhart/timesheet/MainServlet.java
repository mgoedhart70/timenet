package net.goedhart.timesheet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

@SuppressWarnings("serial")
public class MainServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(MainServlet.class);

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			  throws ServletException, IOException {
		String action = req.getParameter("p");
		
		logger.debug("get action=" + action);
		
		if (action == null) action = "";
			
			
		if (action.equals("1")) {
			
			req.getRequestDispatcher("/project_grid.jsp").forward(req, resp);
			
		} else if (action.equals("2")) {
			
			req.getRequestDispatcher("/customer_grid.jsp").forward(req, resp);
			
		} else if (action.equals("3")) {
			
			req.getRequestDispatcher("/work_grid.jsp").forward(req, resp);
			
		} else if (action.equals("4")) {
			
			req.getRequestDispatcher("/report.jsp").forward(req, resp);
			
		} else {
			
			req.getRequestDispatcher("/home.jsp").forward(req, resp);
			
		}
		
	}
	
}
