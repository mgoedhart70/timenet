package net.goedhart.timesheet;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class FloatSerializer implements JsonDeserializer<Float> {

	@Override
	public Float deserialize(JsonElement src, Type typeOfSrc,
			JsonDeserializationContext context) throws JsonParseException {
		
		Float hours;
		try {
			hours = src.getAsFloat();
		} catch (NumberFormatException e) {
			hours = 0f;
		}
		
		return hours;
	}

}
