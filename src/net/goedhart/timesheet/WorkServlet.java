/**
 * Copyright 2011 Google
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.goedhart.timesheet;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.goedhart.timesheet.model.Project;
import net.goedhart.timesheet.model.Task;
import net.goedhart.timesheet.model.Work;

import org.apache.log4j.Logger;

import com.google.appengine.api.NamespaceManager;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * This servlet responds to the request corresponding to project entities. The servlet
 * manages the customer Entity
 * 
 * 
 */
@SuppressWarnings("serial")
public class WorkServlet extends BaseServlet {

    private static final Logger logger = Logger.getLogger(WorkServlet.class);
    
	private SimpleDateFormat date_format = new SimpleDateFormat("dd-MM-yyyy");
	private Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy").create();
//	private int tz = 0;

	
	protected void doList(HttpServletRequest req, HttpServletResponse resp, User user)
			throws ServletException, IOException {
		
		logger.debug("Obtaining work listing");
		
//		logger.debug("sord=" + req.getParameter("sord"));
//		logger.debug("page=" + req.getParameter("page"));
//		logger.debug("sidx=" + req.getParameter("sidx"));
//		logger.debug("_search=" + req.getParameter("_search"));
		logger.debug("rows=" + req.getParameter("rows"));
		
		int startIndex = 0;
		int totalRecords = 0;
		int page = Integer.parseInt(req.getParameter("page"));
		int pageSize = Integer.parseInt(req.getParameter("rows"));
		String sortfield =  req.getParameter("sidx");
		
		boolean ascending = false;
		if (req.getParameter("sord").equalsIgnoreCase("asc")) ascending = true;

		// get monday date from date
		Date date = null;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
		String sdate = req.getParameter("date");
		try {
			
			date = sdf.parse(sdate);
			if (date == null) date = new Date();
			cal.setTime(date);
			
		} catch (ParseException e) {
			
			cal.setTime(new Date());
		}
		
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		date = cal.getTime();
		
		
		PrintWriter out = resp.getWriter();
		
		startIndex = pageSize * page - pageSize;
		
		List<Project> projects = Project.list(date);

		List<Week> weeks = new ArrayList<Week>();
		
		Week total = new Week("Total", "", date, -1);
		total.init0();
		
		// Calendar.SUNDAY = 1
		for (Project p: projects) {
//			logger.debug("projid="+p.getId());
			
			List<Task> tasks = Task.list(p.getId());
			
			for (Task t: tasks) {
//				logger.debug("taskid=" + t.getId());
				
				Week week = new Week(p.getName(),
						          t.getName(),
						          date,
						          t.getId());
				
				List<Work> works = Work.week(date, t, user);
//				logger.debug("work size=" + works.size());
				for (Work work: works) {
					
//					logger.debug(t.getName()+ " - " + work.getHours() + " - " + work.getDate());

					cal.setTime(work.getDate());
					
					if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
						week.mon_hours = work.getHours();
						total.mon_hours += week.mon_hours;
						week.tot_hours += week.mon_hours;
					}
					if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY) {
						week.tue_hours = work.getHours();
						total.tue_hours += week.tue_hours;
						week.tot_hours += week.tue_hours;
					}
					if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) {
						week.wed_hours = work.getHours();
						total.wed_hours += week.wed_hours;
						week.tot_hours += week.wed_hours;
					}
					if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) {
						week.thu_hours = work.getHours();
						total.thu_hours += week.thu_hours;
						week.tot_hours += week.thu_hours;
					}
					if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
						week.fri_hours = work.getHours();
						total.fri_hours += week.fri_hours;
						week.tot_hours += week.fri_hours;
					}
					if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
						week.sat_hours = work.getHours();
						total.sat_hours += week.sat_hours;
						week.tot_hours += week.sat_hours;
					}
					if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
						week.sun_hours = work.getHours();
						total.sun_hours += week.sun_hours;
						week.tot_hours += week.sun_hours;
					}
					
				}
				weeks.add(week);
			}
		}
		
		weeks.add(total);
		
  		Map<String, Object> record = new HashMap<String, Object>();
  		
  		totalRecords = weeks.size();
  		record.put("currentpage", page);
  		record.put("totalpages", totalRecords / pageSize + 1);
		record.put("totalrecords", totalRecords);
		record.put("mondate", date);

		record.put("weeks", weeks);

		logger.debug("result:" + gson.toJson(record));
		
		out.println(gson.toJson(record));
		
	}

	protected void doReport(ReportQuery reportQuery, HttpServletResponse resp, User user)
			throws ServletException, IOException {
		
		PrintWriter out = resp.getWriter();

		logger.debug("Obtaining report");
		
		Gson gsonb = new GsonBuilder()
			.setDateFormat("dd-MM-yyyy")
			.create();

		List<Report> reports = Report.fillReport(reportQuery, user);

		Map<String, Object> m = new HashMap<String, Object>();
		m.put("reports", reports);
		logger.debug("reports=" + gsonb.toJson(m));
		

		out.println(gsonb.toJson(m));
		
	}
	

	/**
	 * Update the Work entity
	 */
	protected void doUpdate(Week week, HttpServletResponse resp, User user)
			throws ServletException, IOException {
		
		PrintWriter out = resp.getWriter();
  		Map<String, Object> record = new HashMap<String, Object>();
		
  		Calendar cal = Calendar.getInstance();
  		
  		cal.setFirstDayOfWeek(Calendar.MONDAY);
  		cal.setTime(week.monday);
  		
		String result = "error";
		
		try {
  		
	  		Work work = null;
	  		work = new Work(week.monday, week.task_id, week.mon_hours);
	  		work.update(user);
	  		
	 		cal.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
	  		work = new Work(cal.getTime(), week.task_id, week.tue_hours);
	  		work.update(user);
	  		
	  		cal.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
	  		work = new Work(cal.getTime(), week.task_id, week.wed_hours);
	  		work.update(user);
	  		
			cal.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
	  		work = new Work(cal.getTime(), week.task_id, week.thu_hours);
	  		work.update(user);
	  		
	  		cal.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
	  		work = new Work(cal.getTime(), week.task_id, week.fri_hours);
	  		work.update(user);
	  		
	  		cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
	  		work = new Work(cal.getTime(), week.task_id, week.sat_hours);
	  		work.update(user);
	  		
	  		cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
	  		work = new Work(cal.getTime(), week.task_id, week.sun_hours);
	  		work.update(user);
  		
			result = "success";
			

		} catch(Exception e) {
			
			result = e.getMessage();
			logger.error(result);
			
		}
			
		logger.error("result=" + result);
		
		if (result.equals("success"))
			out.println(result);
//			out.println(gson.toJson(record));
		else
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, result);

	}


	/**
	 * Redirect the call to doDelete or doPut method
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		super.doPost(req, resp);
		
		logger.debug("namespace="+NamespaceManager.get());
		
		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();

		
		String action = req.getParameter("oper");
		
		logger.debug("oper=" + action);

		// put post parameters in map for json 
		Map<String, Object> record = new HashMap<String, Object>();
		
		@SuppressWarnings("unchecked")
		Enumeration<String> enume = req.getParameterNames();
		while (enume.hasMoreElements()) {
			String s = enume.nextElement();
			record.put(s, req.getParameter(s));
		}

		// put empty date as null in project
		GsonBuilder gsonBuilder = new GsonBuilder();
	    gsonBuilder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

	        @Override
	        public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
	                throws JsonParseException {
	            try {
	                return date_format.parse(json.getAsString());
	            } catch (ParseException e) {
	                return null;
	            }
	        }
	    });
	    Gson gson = gsonBuilder.registerTypeAdapter(Float.class, new FloatSerializer()).create();
	    
		String querystring = gson.toJson(record);
		logger.debug("querystring=" + querystring);

		
		if (action == null || action.isEmpty()) {
			doList(req, resp, user);
		} else if (action.equalsIgnoreCase("edit")) {
			Week week = gson.fromJson(gson.toJson(record), Week.class);
			logger.debug("week:" + gson.toJson(week));
			doUpdate(week, resp, user);
			return;
		} else if (action.equalsIgnoreCase("report")) {
			ReportQuery rq = gson.fromJson(gson.toJson(record), ReportQuery.class);
			logger.debug("reportQuery: " + gson.toJson(rq));
			doReport(rq, resp, user);
			return;
		} else {
			doList(req, resp, user);
		}
	}

}

