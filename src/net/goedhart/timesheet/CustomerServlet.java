/**
 * Copyright 2011 Google
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.goedhart.timesheet;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.goedhart.timesheet.model.Customer;

import org.apache.log4j.Logger;

import com.google.appengine.api.NamespaceManager;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * This servlet responds to the request corresponding to customer entities. The servlet
 * manages the customer Entity
 * 
 * 
 */
@SuppressWarnings("serial")
public class CustomerServlet extends BaseServlet {

    private static final Logger logger = Logger.getLogger(CustomerServlet.class);
    
	private SimpleDateFormat date_format = new SimpleDateFormat("dd-MM-yyyy");
	private Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy").create();
	
	private enum Operation { add, edit, del };



	protected void doList(HttpServletRequest req, HttpServletResponse resp, User user)
			throws ServletException, IOException {
		
		logger.debug("Obtaining customer listing");
		
		logger.debug("sord=" + req.getParameter("sord"));
		logger.debug("page=" + req.getParameter("page"));
		logger.debug("sidx=" + req.getParameter("sidx"));
		logger.debug("_search=" + req.getParameter("_search"));
		logger.debug("rows=" + req.getParameter("rows"));
		
		int startIndex = 0;
		int totalRecords = 0;
		int page = Integer.parseInt(req.getParameter("page"));
		int pageSize = Integer.parseInt(req.getParameter("rows"));
		String sortfield =  req.getParameter("sidx");
		
		boolean ascending = false;
		if (req.getParameter("sord").equalsIgnoreCase("asc")) ascending = true;
			
		logger.debug("sort=" + sortfield + ascending);
		
		
		PrintWriter out = resp.getWriter();
		
		startIndex = pageSize * page - pageSize;
		List<Customer> list = Customer.list(startIndex, pageSize, sortfield, ascending);

		Gson gsonb = new GsonBuilder().setDateFormat("yyyy-MM-dd hh:mm:ss").create();
  		Map<String, Object> record = new HashMap<String, Object>();
  		
  		totalRecords = Customer.size();
  		record.put("currentpage", page);
  		record.put("totalpages", totalRecords / pageSize + 1);
		record.put("totalrecords", totalRecords);

		record.put("customers", list);

		logger.debug("result:" + gsonb.toJson(record));
		
		out.println(gsonb.toJson(record));
		
	}

	
	protected void doOptions(HttpServletRequest req, HttpServletResponse resp, User user)
			throws ServletException, IOException {
		
		logger.debug("Obtaining customer options");
		
		logger.debug("customer_id=" + req.getParameter("customer_id"));
		
		
		PrintWriter out = resp.getWriter();

		List<Customer> custlist = Customer.options();

		Gson gson = new Gson();
  		Map<Long, Object> record = new HashMap<Long, Object>();
  		
		for (Customer cust : custlist) {
			record.put(cust.getId(), cust.getName());
		}

		logger.debug("result:" + gson.toJson(record));
		
		out.println(gson.toJson(record));
			
	}
	
	/**
	 * Create the customer entity
	 */
	protected void doCreate(Customer cust, HttpServletResponse resp, User user)
			throws ServletException, IOException {
		
		logger.debug("Creating customer");
		PrintWriter out = resp.getWriter();
		String result = "";
		
		try {
			
			cust.create(user);
			
			result = "success";
				
		} catch (Exception e) {
			
			result = e.getMessage();
			logger.error(result);
		}

		if (result.equals("success"))
			out.println(result);
		else
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, result);

	}

	/**
	 * Update the customer entity
	 */
	protected void doUpdate(Customer cust, HttpServletResponse resp, User user)
			throws ServletException, IOException {
		
		logger.debug("Updating customer");
		PrintWriter out = resp.getWriter();
		String result = "";
		
		try {
			
			cust.update(user);
			
			result = "success";

		} catch(Exception e) {

			result = e.getMessage();
			logger.error(result);
		}
			
		if (result.equals("success"))
			out.println(result);
		else
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, result);

	}

	/**
	 * Delete the customer entity
	 */
	protected void doDelete(Customer cust, HttpServletResponse resp, User user)
			throws ServletException, IOException {
		
		PrintWriter out = resp.getWriter();
		String result = "";
		
		try {
			cust.delete();
			
			result = "success";

		} catch(Exception e) {
			
			result = e.getMessage();
			logger.error(result);
		}
		
		if (result.equals("success")) 
			out.println(result);
		else
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, result);

	}

	/**
	 * Redirect the call to doDelete or doPut method
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		super.doPost(req, resp);
		
		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();
		
		logger.debug("namespace="+NamespaceManager.get());
		
		String operation = req.getParameter("oper");

		logger.info("operation=" + operation);
		
		// put post parameters in map for json 
		Map<String, Object> record = new HashMap<String, Object>();
		
		@SuppressWarnings("unchecked")
		Enumeration<String> enume = req.getParameterNames();
		while (enume.hasMoreElements()) {
			String s = enume.nextElement();
			
			// when add operation customer_id equals _empty
			if (s.equalsIgnoreCase("customer_id")) {
				try {
					long id = Long.valueOf(req.getParameter(s));
					record.put(s, id);
				} catch (NumberFormatException e) {}
			} else
				record.put(s, req.getParameter(s));
		}

		// put empty date as null in customer
		GsonBuilder gsonBuilder = new GsonBuilder();
	    gsonBuilder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

	        @Override
	        public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
	                throws JsonParseException {
	            try {
	                return date_format.parse(json.getAsString());
	            } catch (ParseException e) {
	                return null;
	            }
	        }
	    });
	    Gson gson = gsonBuilder.create();
	    
		String querystring = gson.toJson(record);
		logger.debug("querystring=" + querystring);

		if (operation == null) operation = "list";
		
		Customer cust = null;
		try {
			Operation.valueOf(operation);
			cust = gson.fromJson(gson.toJson(record), Customer.class);
		} catch (Exception e) {}
	

		if (operation.equalsIgnoreCase("del")) {
			doDelete(cust, resp, user);
			return;
		} else if (operation.equalsIgnoreCase("add")) {
			doCreate(cust, resp, user);
			return;
		} else if (operation.equalsIgnoreCase("edit")) {
			doUpdate(cust, resp, user);
			return;
		} else if (operation.equalsIgnoreCase("options")) {
			doOptions(req, resp, user);
			return;
		} else {
			doList(req, resp, user);
		}
	}

}

