package net.goedhart.timesheet.model;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import net.goedhart.timesheet.Tools;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.QueryResultIterable;
import com.google.appengine.api.users.User;
import com.google.gson.annotations.SerializedName;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;
import com.googlecode.objectify.annotation.Parent;

import com.googlecode.objectify.cmd.Query;

import static net.goedhart.timesheet.OfyService.ofy;

@Entity
public class Work {

	private enum Field {
		customer_id, customer_name, timestamp;
	}

    private static final Logger logger = Logger.getLogger(Work.class);

	@SerializedName("work_id") @Id private Long id;

	@SerializedName("task_id") @Ignore private Long task_id;
	@Index @Load transient private Ref<Task> task;
	
	@Index private Date date;
	
	private float hour;
	
	@SuppressWarnings("unused")
	@Index transient private User user;
	
	@SuppressWarnings("unused")
	private String timestamp;

	protected Work() {}

	
	public Work(Date day, long task_id, float hours) {
		
		this.date = day;
		this.hour = hours;
		this.task_id = task_id;
		
		
	}

	protected long getId () {
		return this.id;
	}
	
	public Date getDate() {
		return this.date;
	}
	
	public long getTaskid() {
		return this.task.get().getId();
	}
	
	public float getHours() {
		return this.hour;
	}
	
	public void update (User user) {
		
		this.validate();
		
		// search for work with taskid and date !!
		Work w = ofy().load().type(Work.class)
				.filter("task", this.task)
				.filter("user", user)
				.filter("date", this.date).first().now();
		
		if (w != null) {
			
			if ( !w.timestamp.equals(this.timestamp)) {
				//throw new IllegalArgumentException("Timestamp mismatch");
			}
			
			// if hours equals 0 then delete
			if (this.hour == 0)
				w.delete();
			
			return;
			
		} else {
			
			// if new and hours equals 0 then nothing
			if (this.hour == 0)
				return;
		}
		
		
		w = this;
			
		w.save(user);
				
		logger.info("Work " + w.id + " updated");
		
	}
	
	private void validate() {
		
		if (this.hour < 0 || this.hour > 24) {
			throw new IllegalArgumentException("Hours must be less than or equal to 24");
		}
		
		Task task = ofy().load().type(Task.class).id(task_id).now();
		if (task == null) {
			throw new IllegalArgumentException("Unknown task");
		}
		
//		logger.debug("task_id=" + this.task_id);
		
		this.task = Ref.create(task);

	}
	
	private void save (User user) {
		
		this.user = user;
		this.timestamp = Tools.timestamp();

		
		ofy().save().entity(this).now();
		
	}
	
	
	protected void delete () {
		
		Work w = ofy().load().type(Work.class).id(this.id).now();
		
		if (w != null) {
			
			ofy().delete().entity(w).now();
				
			logger.info("Work " + w.id + " deleted");
			
		} else {
			throw new IllegalArgumentException("Work doesn't exist");
		}
		
	}

	public static List<Work> week(Date date, Task task, User user) {
		
		if (date == null) date = new Date();
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
  		cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);

		date = cal.getTime();
		cal.add(Calendar.DAY_OF_MONTH, 7);
		
		Date until = cal.getTime();
		
		
		List<Work> works = ofy().load().type(Work.class)
				.filter("date >=", date)
				.filter("date <=", until)
				.filter("task", task)
				.filter("user", user)
				.list();
		
//		logger.debug("size works=" + works.size());
		
		return works;
		
	}
	
	public static List<Work> week(Date from, Date until, User user) {
		
		logger.debug(from + " - " + until);
		
		if (from == null || until == null) return null;
		
		List<Work> works = ofy().load().type(Work.class)
				.filter("date >=", from)
				.filter("date <=", until)
				.filter("user", user)
				.list();
		
		logger.debug("size works=" + works.size());
		
		return works;
		
	}

	public static List<Work> report(Date from, Date until, User user, long task_id) {
		
		if (from == null) from = new Date();
		
		Query<Work> qwork = ofy().load().type(Work.class);

		qwork = qwork
				.filter("date >=", from);
		
		if (until != null) qwork = qwork.filter("date <=", until);
		
		if (task_id > 0) {
			Task task = ofy().load().type(Task.class).id(task_id).now();
			if (task != null) qwork = qwork.filter("task", task);
		}
		if (user != null) qwork = qwork	.filter("user", user);
	
		List<Work> works = qwork.list();

		return works;
	}
	
}
