package net.goedhart.timesheet.model;

import java.util.*;

import net.goedhart.timesheet.Tools;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.users.User;
import com.google.gson.annotations.SerializedName;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

import com.googlecode.objectify.cmd.Query;

import static net.goedhart.timesheet.OfyService.ofy;

@Entity
public class Task {

	private enum Field {
		project_id, task_id, project_name, timestamp;
	}

    private static final Logger logger = Logger.getLogger(Task.class);
    
	@SerializedName("task_id") @Id private Long id;

	@SerializedName("project_id") @Ignore private Long project_id;
	@Index @Load transient protected Ref<Project> project;
	
	
	@SerializedName("task_name") private String name;
	@SuppressWarnings("unused")
	@Index transient private String caseInsensitiveName; 
	
	private boolean billable;
	
	private boolean holiday;
	
	@SuppressWarnings("unused")
	private transient User user;
	
	@SuppressWarnings("unused")
	private Float wage;
	
	private String timestamp;
	
	private Task() {	}
	
	public long getProjectId() {
		return this.project.get().getId();
	}
	
	public String getProjectName() {
		return this.project.get().getName();
	}
	

	public long getId () {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public float getWage() {
		return this.wage;
	}
	
	public void create (User user) {
		
		this.validate();

		this.save(user);
		
		logger.info("Task " + this.id + " created");
		
	}

	public void update (User user) {
		
		this.validate();
		
		Task t = ofy().load().type(Task.class).id(this.id).now();
		
		if (t != null) {
			
			if (!t.timestamp.equals(this.timestamp)) {
				throw new IllegalArgumentException("Timestamp mismatch");
			}
			
			t = this;
			
			t.save(user);
				
			logger.info("Task " + t.id + " updated");
			
		} else {
			throw new IllegalArgumentException("Project doesn't exist");
		}
		
	}
	
	private void validate() {
		
		if (this.name == null || this.name.trim().equals("")) {
			throw new IllegalArgumentException("Task name is required");
		}
		
		
		Project p = ofy().load().type(Project.class).id(this.project_id).now();
		
		Task result = ofy().load().type(Task.class)
				.filter("caseInsensitiveName", this.name.toLowerCase())
				.filter("project", p)
				.first().now();
		if (result != null && result.id != this.id) {
			throw new IllegalArgumentException("Task already exists with this name");
		}
		
	}
	
	private void save (User user) {
		
		this.user = user;
		this.timestamp = Tools.timestamp();
		this.caseInsensitiveName = this.name.toLowerCase();

		Project p = ofy().load().type(Project.class).id(this.project_id).now();
		
		logger.debug("project_id=" + this.project_id);
		
		this.project = Ref.create(p);
		
		ofy().save().entity(this).now();
		
		
	}
	
	public static String getDeSerializedName(String serializedName) {
		
		String deserializedName = null;
		
		if (serializedName == null) return null;
		
		try {
			Field field = Field.valueOf(serializedName);

			switch(field) {
			default:
				deserializedName = "caseInsensitiveName";
				break;
			}
		} catch (Exception e) {}
		
		return deserializedName;
	}
	
	public void delete () {
		
		Task t = ofy().load().type(Task.class).id(this.id).now();
		
		if (t != null) {
			
			if (t.timestamp != this.timestamp) {
//				throw new IllegalArgumentException("Timestamp mismatch");
			}
			
			Work w = ofy().load().type(Work.class)
					.filter("task", t)
					.first().now();
			if (w != null) {
				throw new IllegalArgumentException("Work still exists in this task");
			}

			ofy().delete().entity(t).now();
				
			logger.info("Task " + t.id + " deleted");
			
		} else {
			throw new IllegalArgumentException("Task doesn't exist");
		}
		
	}
	
	public static int size() {
		
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();

		com.google.appengine.api.datastore.Query q = new com.google.appengine.api.datastore.Query("Task");
		q.setKeysOnly();
		int totalEntities = ds.prepare(q).countEntities(FetchOptions.Builder.withDefaults());
		
		return totalEntities;
	}

	public static List<Task> list(long project_id) {

		String sortOrder = "caseInsensitiveName";
		
		Project p = null;
		if (project_id > 0) {
			p = ofy().load().type(Project.class).id(project_id).now();
		}
		
		Query<Task> qtask = ofy().load().type(Task.class)
				.order(sortOrder);
		
		if (p != null) qtask = qtask.filter("project", p);
		
		List<Task> tasks = qtask.list();
		
		return tasks;
		
	}

	public static List<Task> list(int startIndex, 
            int pageSize, 
            String sortfield,
            boolean ascending) {

		String sortOrder;
		
		logger.debug("sortfield=" + sortfield);
		
		sortOrder = Task.getDeSerializedName(sortfield);
		if (sortfield == null) {
		sortOrder = "caseInsensitiveName";
		} else {
		if (!ascending) sortOrder = "-" + sortOrder;
		}
		
		logger.debug("sortOrder=" + sortOrder);
		
		List<Task> tasks = ofy().load().type(Task.class)
				.offset(startIndex)
				.limit(pageSize)
				.order(sortOrder)
				.list();
		
		return tasks;
		
	}

}
