package net.goedhart.timesheet.model;

import java.util.List;

import net.goedhart.timesheet.Tools;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.users.User;
import com.google.gson.annotations.SerializedName;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.cmd.Query;

import static net.goedhart.timesheet.OfyService.ofy;

@Entity
public class Customer {

	private enum Field {
		customer_id, customer_name, timestamp;
	}

    private static final Logger logger = Logger.getLogger(Customer.class);

	@SerializedName("customer_id") @Id private Long id;
	
	@SerializedName("customer_name") private String name;
	@SuppressWarnings("unused")
	@Index transient private String caseInsensitiveName; 
	
	@SuppressWarnings("unused")
	@SerializedName("customer_street") private String street;
	
	@SuppressWarnings("unused")
	@SerializedName("customer_zipcode") private String zipcode;

	@SuppressWarnings("unused")
	@SerializedName("customer_city") private String city;
	
	@SuppressWarnings("unused")
	private transient User user;
	
	@SuppressWarnings("unused")
	private String timestamp;

	protected Customer() {}
	
	
	public long getId () {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public static String getName(long customer_id) {
		
		Customer c = ofy().load().type(Customer.class).id(customer_id).now();
		
		if (c != null) 	return c.name;
		
		return null;
	}

	public void create (User user) {
		
		this.validate();

		this.save(user);
		
		logger.info("Customer " + this.id + " created");
		
	}

	public void update (User user) {
		
		this.validate();
		
		Customer c = ofy().load().type(Customer.class).id(this.id).now();
		
		if (c != null) {
			
			if (!c.timestamp.equals(this.timestamp)) {
				throw new IllegalArgumentException("Timestamp mismatch");
			}

			c = this;
			
			c.save(user);
				
			logger.info("Customer " + c.id + " updated");
			
		} else {
			throw new IllegalArgumentException("Customer doesn't exist");
		}
		
	}
	
	private void validate() {
		
		if (this.name == null || this.name.trim().equals("")) {
			throw new IllegalArgumentException("Customer name is required");
		}
		
		Customer result = ofy().load().type(Customer.class).filter("caseInsensitiveName", 
				this.name.toLowerCase()).first().now();
		if (result != null && result.id != this.id) {
			logger.info("Customer already exists");
			throw new IllegalArgumentException("Customer already exists with this name");
		}
	}
	
	private void save (User user) {
		
		this.user = user;
		this.timestamp = Tools.timestamp();
		this.caseInsensitiveName = this.name.toLowerCase();

		ofy().save().entity(this).now();
		
	}
	
	private static String getDeSerializedName(String serializedName) {
		
		String deserializedName = null;
		
		if (serializedName == null) return null;
		
		try {
			Field field = Field.valueOf(serializedName);

			switch(field) {
			default:
				deserializedName = "caseInsensitiveName";
				break;
			}
		} catch (Exception e) {}
		
		return deserializedName;
	}
	
	public void delete () {
		
		Customer c = ofy().load().type(Customer.class).id(this.id).now();
		
		if (c != null) {
			
			ofy().delete().entity(c).now();
				
			logger.info("Customer " + c.id + " deleted");
			
		} else {
			throw new IllegalArgumentException("Customer doesn't exist");
		}
		
	}
	
	public static int size() {
		
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();

		com.google.appengine.api.datastore.Query q = new com.google.appengine.api.datastore.Query("Customer");
		q.setKeysOnly();
		int totalEntities = ds.prepare(q).countEntities(FetchOptions.Builder.withDefaults());
		
		return totalEntities;
	}

	public static List<Customer> list(int startIndex, 
                                      int pageSize,
                                      String sortfield,
                                      boolean ascending) {

		String sortOrder;
		
		logger.debug("sortfield=" + sortfield);
		
		sortOrder = Customer.getDeSerializedName(sortfield);
		if (sortfield == null) {
			sortOrder = "caseInsensitiveName";
		} else {
			if (!ascending) sortOrder = "-" + sortOrder;
		}
		
		logger.debug("sortOrder=" + sortOrder);
		
		Query<Customer> qcustomer = ofy().load().type(Customer.class);
		
		if (pageSize > 0) {
			qcustomer = qcustomer
				.offset(startIndex)
				.limit(pageSize);
		}
		
		if (sortOrder.equals("")) {
			qcustomer = qcustomer
				.order(sortOrder);
		}
		
		List<Customer> customers = qcustomer.list();
		
		logger.debug("size cust=" + customers.size());
		
		return customers;
		
	}
	
	public static List<Customer> options() {
		
		
		String sortOrder = "caseInsensitiveName";
		
		logger.debug("sortOrder=" + sortOrder);
		
		List<Customer> customers = ofy().load().type(Customer.class)
						.order(sortOrder)
						.list();
		
		return customers;
		
	}
}
