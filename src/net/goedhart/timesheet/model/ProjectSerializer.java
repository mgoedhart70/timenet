package net.goedhart.timesheet.model;

import java.lang.reflect.Type;


import com.google.gson.*;

public class ProjectSerializer implements JsonSerializer<Project> {

	@Override
	public JsonElement serialize(Project src, Type typeOfSrc, JsonSerializationContext context) {
		
		Gson gson = new GsonBuilder()
					.setDateFormat("yyyy-MM-dd hh:mm:ss")
					.create();
		JsonElement elem = gson.toJsonTree(src);
		
		JsonObject json = elem.getAsJsonObject();
		json.addProperty("customer_id", src.getCustomerId());
		
		return elem;

	}

}
