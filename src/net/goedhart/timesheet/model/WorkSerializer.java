package net.goedhart.timesheet.model;

import java.lang.reflect.Type;

import net.goedhart.timesheet.model.Task;

import com.google.gson.*;

public class WorkSerializer implements JsonSerializer<Work> {

	public JsonElement serialize(Work src, Type typeOfSrc, JsonSerializationContext context) {
		
		Gson gson = new GsonBuilder()
					.setDateFormat("yyyy-MM-dd hh:mm:ss")
					.create();
		JsonElement elem = gson.toJsonTree(src);
		
		JsonObject json = elem.getAsJsonObject();
		json.addProperty("task_id", src.getTaskid());
		
		return elem;

	}

}
