package net.goedhart.timesheet.model;

import java.lang.reflect.Type;

import net.goedhart.timesheet.model.Task;

import com.google.gson.*;

public class TaskSerializer implements JsonSerializer<Task> {

	public JsonElement serialize(Task src, Type typeOfSrc, JsonSerializationContext context) {
		
		Gson gson = new GsonBuilder()
					.setDateFormat("yyyy-MM-dd hh:mm:ss")
					.create();
		JsonElement elem = gson.toJsonTree(src);
		
		JsonObject json = elem.getAsJsonObject();
		json.addProperty("project_id", src.getProjectId());
		
		return elem;

	}

}
