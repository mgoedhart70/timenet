package net.goedhart.timesheet.model;

import java.util.*;

import net.goedhart.timesheet.Tools;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.users.User;
import com.google.gson.annotations.SerializedName;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;
import com.googlecode.objectify.cmd.Query;

import static net.goedhart.timesheet.OfyService.ofy;

@Entity
public class Project {

	private enum Field {
		project_id, project_name, project_description, project_begin, project_end, timestamp;
	}

    private static final Logger logger = Logger.getLogger(Project.class);
    
	@SerializedName("project_id") @Id private Long id;
	
	@SerializedName("project_name") private String name;
	@SuppressWarnings("unused")
	@Index transient private String caseInsensitiveName; 
	
	@SerializedName("project_description") private String description;
	
	@Index @SerializedName("project_begin") private Date startDate;

	@Index @SerializedName("project_end") private Date endDate;
	
	@SuppressWarnings("unused")
	private transient User user;
	
	private String timestamp;
	
	@SerializedName("customer_id") @Ignore private Long customer_id;
	@Index @Load transient protected Ref<Customer> customer;
	
	private Project() {	}

	public long getId () {
		return this.id;
	}
	
	public String getDescription() {
		return this.description;
	}
	public Date getStartDate() {
		return this.startDate;
	}

	public long getCustomerId() {
		return this.customer.get().getId();
	}
	
	protected String getCustomerName() {
		return this.customer.get().getName();
	}
	
	public String getName() {
		return this.name;
	}
	
	public static String getName(long project_id) {
		Project p = ofy().load().type(Project.class).id(project_id).now();
		
		if (p != null) return p.name;
		
		return null;
	}
	
	public void create (User user) {
		
		this.validate();

		this.save(user);
		
		logger.info("Project " + this.id + " created");
		
	}

	public void update (User user) {
		
		this.validate();
		
		Project p = ofy().load().type(Project.class).id(this.id).now();
		
		if (p != null) {
			
			if (!p.timestamp.equals(this.timestamp)) {
				throw new IllegalArgumentException("Timestamp mismatch");
			}
			
			p = this;
			
			p.save(user);
				
			logger.info("Project " + p.id + " updated");
			
		} else {
			throw new IllegalArgumentException("Project doesn't exist");
		}
		
	}
	
	private void validate() {
		
		if (this.name == null || this.name.trim().equals("")) {
			throw new IllegalArgumentException("Project name is required");
		}
		
		if (this.startDate == null) {
			throw new IllegalArgumentException("Begin date project is required");
		}
		
		if (this.endDate != null) {
			if (this.startDate.after(this.endDate)) {
				throw new IllegalArgumentException("Begin date is after end date");
			}
		}
		
		if (this.description == null || this.description.equals("")) {
			throw new IllegalArgumentException("Project description is required");
		}
		
		Project result = ofy().load().type(Project.class).filter("caseInsensitiveName", 
				this.name.toLowerCase()).first().now();
		if (result != null && result.id != this.id) {
			throw new IllegalArgumentException("Project already exists with this name");
		}
		
	}
	
	private void save (User user) {
		
		this.user = user;
		this.timestamp = Tools.timestamp();
		this.caseInsensitiveName = this.name.toLowerCase();

		if (this.customer_id == null) {
			throw new IllegalArgumentException("No customer specified");
		}
		
		Customer c = ofy().load().type(Customer.class).id(this.customer_id).now();
		
		logger.debug("customer_id=" + this.customer_id);
		
		this.customer = Ref.create(c);

		ofy().save().entity(this).now();
		
	}
	
	public static String getDeSerializedName(String serializedName) {
		
		String deserializedName = null;
		
		if (serializedName == null) return null;
		
		try {
			Field field = Field.valueOf(serializedName);

			switch(field) {
			case project_description:
				deserializedName = "description";
				break;
			case project_begin:
				deserializedName = "startDate";
				break;
			case project_end:
				deserializedName = "endDate";
				break;
			default:
				deserializedName = "caseInsensitiveName";
				break;
			}
		} catch (Exception e) {}
		
		return deserializedName;
	}
	
	public void delete () {
		
		Project p = ofy().load().type(Project.class).id(this.id).now();
		
		if (p != null) {
			
			if (p.timestamp != this.timestamp) {
//				throw new IllegalArgumentException("Timestamp mismatch");
			}
			
			Task t = ofy().load().type(Task.class)
					.filter("project", p)
					.first().now();
			if (t != null) {
				throw new IllegalArgumentException("Tasks still exists in this project");
			}
			
			ofy().delete().entity(p).now();
				
			logger.info("Project " + p.id + " deleted");
			
		} else {
			throw new IllegalArgumentException("Project doesn't exist");
		}
		
	}
	
	public static int size() {
		
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();

		com.google.appengine.api.datastore.Query q = 
				new com.google.appengine.api.datastore.Query("Project");
		q.setKeysOnly();
		int totalEntities = ds.prepare(q).countEntities(FetchOptions.Builder.withDefaults());
		
		return totalEntities;
	}
	
	public static List<Project> list(int startIndex, 
            int pageSize, 
            String sortfield,
            boolean ascending) {

		String sortOrder;
		
		logger.debug("sortfield=" + sortfield);
		
		sortOrder = Project.getDeSerializedName(sortfield);
		if (sortfield == null) {
			sortOrder = "caseInsensitiveName";
		} else {
			if (!ascending) sortOrder = "-" + sortOrder;
		}
		
		logger.debug("sortOrder=" + sortOrder);

		List<Project> projects = ofy().load().type(Project.class)
				.offset(startIndex)
				.limit(pageSize)
				.order(sortOrder)
				.list();
		
		return projects;
		
	}

	public static List<Project> list(Date date) {

		Query<Project> q = ofy().load().type(Project.class);
		
		if (date != null) q = q.filter("startDate <=", date);
		
		List<Project> projects = q.list();

		for (Project p: projects) {
			if (p.endDate != null && date != null && p.endDate.before(date)) {
				projects.remove(p);
			}
		}
		return projects;
		
	}
}
