package net.goedhart.timesheet.model;

import java.lang.reflect.Type;


import org.apache.log4j.Logger;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.googlecode.objectify.Ref;

public class CustomerSerializer implements JsonSerializer<Ref<Customer>> {
	
	@Override
	public JsonElement serialize(Ref<Customer> src, Type typeOfSrc, JsonSerializationContext context) {
		
		JsonObject obj = new JsonObject();
		
		obj.addProperty("customer_id", src.get().getId());

		return obj;
	}

}
