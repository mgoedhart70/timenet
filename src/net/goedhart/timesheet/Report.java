package net.goedhart.timesheet;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import net.goedhart.timesheet.model.*;

import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.QueryResultIterable;
import com.google.appengine.api.users.User;
import com.google.apphosting.api.DatastorePb.QueryResult;

import static net.goedhart.timesheet.OfyService.ofy;

public class Report {

    private static final Logger logger = Logger.getLogger(Report.class);
    
	String customer_name;
	String project_name;
	String task_name;
	Date date;
	float hours;
	float wage;
	
	Report (String customer, String project, String task, Date d, float h, float w) {
		this.customer_name = customer;
		this.project_name = project;
		this.task_name = task;
		this.date = d;
		this.hours = h;
		this.wage = w;
	}
	
	public static List<Report> fillReport(ReportQuery reportQuery, User user) {
		
		List<Work> worklist = Work.report(reportQuery.startdate, 
				                          reportQuery.enddate,
				                          user,
				                          reportQuery.task_id );
		
		logger.debug("get projects");
		List<Project> projectlist = Project.list(null);
		logger.debug("get tasks");
		List<Task> tasklist = Task.list(reportQuery.project_id);
		logger.debug("get customers");
		List<Customer> customerlist = Customer.list(0, 0, null, true);
		
		List<Report> reports = new ArrayList<Report>();
		
		
		Iterator<Customer> custiterator = customerlist.iterator();
		Iterator<Project> projiterator;
		Iterator<Task> taskiterator;
		Iterator<Work> workiterator;

		
		while (custiterator.hasNext()) {
			Customer cust = custiterator.next();
			if (reportQuery.customer_id > 0 && cust.getId() != reportQuery.customer_id) continue;
			
			projiterator = projectlist.iterator();
			while (projiterator.hasNext()) {
				Project proj = projiterator.next();
				if (reportQuery.project_id > 0 && proj.getId() != reportQuery.project_id) continue;
				if (cust.getId() != proj.getCustomerId()) continue;
			
				taskiterator = tasklist.iterator();
				while (taskiterator.hasNext()) {
					Task task = taskiterator.next();
					if (reportQuery.task_id > 0 && task.getId() != reportQuery.task_id) continue;
					if (proj.getId() != task.getProjectId()) continue;

					float totalhours = 0;
					
					logger.debug("task=" + task.getName());
					
					workiterator = worklist.iterator();
					while (workiterator.hasNext()) {
						Work work = workiterator.next();
						
						logger.debug("work=" + work.getHours());
						
						if (task.getId() != work.getTaskid()) continue;
						
						logger.debug(" hours=" + work.getHours());
						totalhours += work.getHours();

					}
					Report r = new Report(cust.getName(), proj.getName(), task.getName(), new Date(), 
							totalhours, task.getWage() );
					reports.add(r);
				}
			}
		}
		
		return reports;
	}
}
