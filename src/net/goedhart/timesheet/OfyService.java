package net.goedhart.timesheet;

import net.goedhart.timesheet.model.Customer;
import net.goedhart.timesheet.model.Project;
import net.goedhart.timesheet.model.Task;
import net.goedhart.timesheet.model.Work;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

public class OfyService {

	static {
		ObjectifyService.factory().register(Project.class);
		ObjectifyService.factory().register(Customer.class);
		ObjectifyService.factory().register(Task.class);
		ObjectifyService.factory().register(Work.class);
	}
	
	public static Objectify ofy() {
		return ObjectifyService.ofy();
	}
	
	public static ObjectifyFactory factory() {
		return ObjectifyService.factory();
	}
}
