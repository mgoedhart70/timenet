package net.goedhart.timesheet;

import java.util.Calendar;
import java.util.Date;

class Week {
	
	String project_name;
	String task_name;
	
	long task_id;
	
	Date monday;
	Float mon_hours;
	Float tue_hours;
	Float wed_hours;
	Float thu_hours;
	Float fri_hours;
	Float sat_hours;
	Float sun_hours;
	Float tot_hours = 0f;

	private Week() {}
	
	Week ( String proj, String taskname, Date mon, long task ) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(mon);
		
		if (cal.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
			throw new IllegalArgumentException("This is not a monday");
		}
		
		this.monday = mon;
		this.project_name = proj;
		this.task_name = taskname;
		this.task_id = task;
	}
	
	public void init0 () {
		this.mon_hours = 0f;
		this.tue_hours = 0f;
		this.wed_hours = 0f;
		this.thu_hours = 0f;
		this.fri_hours = 0f;
		this.sat_hours = 0f;
		this.sun_hours = 0f;
	}
}
