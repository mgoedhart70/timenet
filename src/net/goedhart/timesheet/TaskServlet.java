/**
 * Copyright 2011 Google
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.goedhart.timesheet;

import java.io.*;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.goedhart.timesheet.model.Task;
import net.goedhart.timesheet.model.TaskSerializer;

import org.apache.log4j.Logger;

import com.google.appengine.api.NamespaceManager;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * This servlet responds to the request corresponding to project entities. The servlet
 * manages the project Entity
 * 
 * 
 */
@SuppressWarnings("serial")
public class TaskServlet extends BaseServlet {

    private static final Logger logger = Logger.getLogger(TaskServlet.class);
    
	private SimpleDateFormat date_format = new SimpleDateFormat("dd-MM-yyyy");
	private Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyy").create();
	
	protected void doList(HttpServletRequest req, HttpServletResponse resp, User user)
			throws ServletException, IOException {
		
		logger.debug("Obtaining task listing");
		
		Long project_id = 0L;
		try {
			project_id = Long.parseLong(req.getParameter("project_id"));
		} catch (NumberFormatException e) {}
		
		if (project_id == 0) return;

		int startIndex = 0;
		int totalRecords = 0;
		int page = Integer.parseInt(req.getParameter("page"));
		int pageSize = Integer.parseInt(req.getParameter("rows"));
		String sortfield =  req.getParameter("sidx");
		
		boolean ascending = false;
		if (req.getParameter("sord").equalsIgnoreCase("asc")) ascending = true;
			
		logger.debug("sort=" + sortfield + " asc=" + ascending);

		PrintWriter out = resp.getWriter();
		
		startIndex = pageSize * page - pageSize;
		List<Task> list = Task.list(project_id);

		Gson gsonb = new GsonBuilder()
				.setDateFormat("yyyy-MM-dd hh:mm:ss")
				.registerTypeAdapter(Task.class, new TaskSerializer())
				.create();
		
  		Map<String, Object> record = new HashMap<String, Object>();
  		
  		totalRecords = list.size();
  		record.put("currentpage", page);
  		record.put("totalpages", totalRecords / pageSize + 1);
		record.put("totalrecords", totalRecords);

		record.put("tasks", list);

		logger.debug("records:" + gsonb.toJson(record));
		out.println(gsonb.toJson(record));
			
	}

	/**
	 * Create the task entity
	 */
	protected void doCreate(Task task, HttpServletResponse resp, User user)
			throws ServletException, IOException {
		
		logger.debug("Creating task");
		PrintWriter out = resp.getWriter();
		String result = "";
		
		try {
			
			task.create(user);

			result = "success";
				
		} catch (Exception e) {

			result = e.getMessage();
			logger.error(result);
			
		}

		if (result.equals("success"))
			out.println(result);
		else
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, result);

	}

	/**
	 * Update the task entity
	 */
	protected void doUpdate(Task task, HttpServletResponse resp, User user)
			throws ServletException, IOException {
		
		PrintWriter out = resp.getWriter();

		String result = "";
		
		try {
			
			task.update(user);

			result = "success";

		} catch(Exception e) {
			
			result = e.getMessage();
			logger.error(result);
			
		}
			
		if (result.equals("success"))
			out.println(result);
		else
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, result);
		
	}

	/**
	 * Delete the task entity
	 */
	protected void doDelete(Task task, HttpServletResponse resp, User user)
			throws ServletException, IOException {
		
		PrintWriter out = resp.getWriter();
		String result = "";
		
		try {
			task.delete();

			result = "success";
			
		} catch(Exception e) {
			
			result = e.getMessage();
			logger.error(result);
		}
		
		if (result.equals("success"))
			out.println(result);
		else
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, result);
		
	}

	/**
	 * Redirect the call to doDelete, doUpdate, doCreat of list method
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		super.doPost(req, resp);
		
		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();
		
		logger.debug("namespace="+NamespaceManager.get());
		
		String operation = req.getParameter("oper");

		// put post parameters in map for json 
		Map<String, Object> record = new HashMap<String, Object>();
		
		@SuppressWarnings("unchecked")
		Enumeration<String> enume = req.getParameterNames();
		while (enume.hasMoreElements()) {
			String s = enume.nextElement();

			// when add operation task_id equals _empty
			if (s.equalsIgnoreCase("task_id")) {
				try {
					long id = Long.valueOf(req.getParameter(s));
					record.put(s, id);
				} catch (NumberFormatException e) {}
			} else
				record.put(s, req.getParameter(s));
			
		}

		// put empty date as null in project
		GsonBuilder gsonBuilder = new GsonBuilder();
	    gsonBuilder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

	        @Override
	        public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
	                throws JsonParseException {
	            try {
	                return date_format.parse(json.getAsString());
	            } catch (ParseException e) {
	                return null;
	            }
	        }
	    });
	    Gson gson = gsonBuilder.registerTypeAdapter(Float.class, new FloatSerializer()).create();
	    
		if (operation == null) operation = "list";
		
		String querystring = gson.toJson(record);
		logger.debug("querystring=" + querystring);
		
		Task task = gson.fromJson(gson.toJson(record), Task.class);

		if (operation.equalsIgnoreCase("del")) {
			doDelete(task, resp, user);
			return;
		} else if (operation.equalsIgnoreCase("add")) {
			doCreate(task, resp, user);
			return;
		} else if (operation.equalsIgnoreCase("edit")) {
			doUpdate(task, resp, user);
			return;
		} else {
			doList(req, resp, user);
		}
	}

}

