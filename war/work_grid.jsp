<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Timenet</title>
		
		<link rel="shortcut icon" href="/images/timenet.ico" />

		<link href="/stylesheets/main.css" rel="stylesheet" type="text/css" />
	
		<link href="/stylesheets/jquery-ui.smoothness.min.css" rel="stylesheet" type="text/css" />
		
		<link href="/stylesheets/ui.jqgrid.css" rel="stylesheet" type="text/css" />

	 	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
     	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
     	 
		<!-- Include jqGrid script file. -->	
		<script src="/script/grid.locale-en.js" type="text/javascript"></script>
		<script src='/script/jquery.jqGrid.min.js' type="text/javascript"></script>
		    
		<script src='/script/timenet.js' type="text/javascript"></script>
		
		<style type="text/css">
		        div.ui-datepicker { font-size: 11px; }
		</style>
		
		<script type="text/javascript">
		
		    
		    $(document).ready(function () {
		    
		    	jQuery.extend(jQuery.jgrid.edit, {
		    	    savekey: [true, 13],
		    	    closeOnEscape: true,
		    	    closeAfterEdit: true,
		    	    closeAfterAdd: true,
		    	    recreateForm: true
		    	});
			    
		        $('#WorkTable').jqGrid({
		        	url: '/work',
		        	postData: {date: $('#selectWeek').val()},		
		        	datatype: 'json',
		        	mtype: 'post',
		        	height: '300px',
		        	jsonReader: {
		        		root: 'weeks',
		        		records: 'totalrecords',
		        		page: 'currentpage',
		        		total: 'totalpages',
		        		repeatitems: false
		        	},
		        	colNames: ['Id', 'monday', 'Project', 'Task', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun', 'Tot', 'Action' ],
		        	colModel: [
		        	       { name: 'task_id', index: 'task_id', width: 15, key: true, 
		        	    	   editable: false, sortable: false, hidden: true},
		        	       { name: 'monday', index: 'monday', hidden: true, editable: true, sortable: false},
		        	       { name: 'project_name', width: 200, editable: false, editoptions:{size:50}, sortable: false},
		        	       { name: 'task_name', width: 200, editable: false, editoptions:{size:50}, sortable: false},
		        	       
		        	       { name: 'mon_hours', width: 40, editable: true, align: "right", sortable: false, 
		        	    	    	editrules:{number: true, minValue: 0, maxValue: 24}    	
		        	    	},
		        	       { name: 'tue_hours', width: 40, editable: true, align: "right", sortable: false, 
	        	    	    	editrules:{number: true, minValue: 0, maxValue: 24}
		        	    	},
		        	       { name: 'wed_hours', width: 40, editable: true, align: "right", sortable: false, 
	        	    	    	editrules:{number: true, minValue: 0, maxValue: 24}
		        	    	},
		        	       { name: 'thu_hours', width: 40, editable: true, align: "right", sortable: false, 
	        	    	    	editrules:{number: true, minValue: 0, maxValue: 24}
		        	       },
		        	       { name: 'fri_hours', width: 40, editable: true, align: "right", sortable: false, 
	        	    	    	editrules:{number: true, minValue: 0, maxValue: 24}
		        	       },
			        	   { name: 'sat_hours', width: 40, editable: true, align: "right", sortable: false, 
	        	    	    	editrules:{number: true, minValue: 0, maxValue: 24}
		        	       },
			        	   { name: 'sun_hours', width: 40, editable: true, align: "right", sortable: false, 
	        	    	    	editrules:{number: true, minValue: 0, maxValue: 24}
		        	       },
			        	   { name: 'tot_hours', width: 40, editable: false, align: "right", sortable: false },
		        	    	{   name:'act',
		        	    		index:'act',
		        	    		width:55,
		        	    		align:'center',
		        	    		sortable:false,
		        	    		formatter:'actions',
		        	            formatoptions:{
		        	                keys: true, // we want use [Enter] key to save the row and [Esc] to cancel editing.
		        	                delbutton: false,
		        	                onEdit:function(rowid) {
		        	                	if (rowid == -1) return false;
		        	                    // alert("in onEdit: rowid="+rowid+"\nWe don't need return anything");
		        	                },
		        	                onSuccess:function(jqXHR) {
		        	                    // the function will be used as "succesfunc" parameter of editRow function
		        	                    // (see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:inline_editing#editrow)
		        	                     alert("in onSuccess used only for remote editing:"+
		        	                          "\nresponseText="+jqXHR.responseText+
		        	                          "\n\nWe can verify the server response and return false in case of"+
		        	                          " error response. return true confirm that the response is successful"); 
		        	                    // in the case we should return false. In the case onError will be called
		        	                    // we can verify the server response and interpret it do as an error
		        	                    return true;
		        	                },
		        	                onError:function(rowid, jqXHR, textStatus) {
		        	                    // the function will be used as "errorfunc" parameter of editRow function
		        	                    // (see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:inline_editing#editrow)
		        	                    // and saveRow function
		        	                    // (see http://www.trirand.com/jqgridwiki/doku.php?id=wiki:inline_editing#saverow)
		        	                    alert("in onError used only for remote editing:"+
		        	                          "\nresponseText="+jqXHR.responseText+
		        	                          "\nstatus="+jqXHR.status+
		        	                          "\nstatusText"+jqXHR.statusText+
		        	                          "\n\nWe don't need return anything");
		        	                },
		        	                afterSave:function(rowid) {
		        	                    //alert("in afterSave (Submit): rowid="+rowid+"\nWe don't need return anything");
		        	                },
		        	                afterRestore:function(rowid) {
		        	                    //alert("in afterRestore (Cancel): rowid="+rowid+"\nWe don't need return anything");
		        	                }
		        	        }
		        	    }],
				        pager: '#pager1',
		        		rowNum: 10,
		        		viewrecords: true,
		        		caption: 'Hours',
		        		editurl: '/work',
		        		
		        		loadComplete: function(data) {
		        			$(this).jqGrid('setRowData', -1, false, {background: '#D4D4D4'});
		        		}

		        }).navGrid("#pager1", {search: false, add: false, del:false, edit:false}); 
			    
		        
			    $("#selectWeek").datepicker({
			    	
			        firstDay : 1, // set first day of week as monday
			        changeMonth : true, // provide option to select Month
			        changeYear : true, // provide option to select year
			        showWeek: true,
			        dateFormat: 'dd-mm-yy',
			        showButtonPanel : true, // button panel having today and done button
			        startDate: monDate,
			           
			        onClose : function(dateText, inst) {

			                  // below code will calculate the monday and set it in input box
			                  var fromDate = $(this).datepicker("getDate");
			                  
			  			      setDates(monDate(fromDate));

			                  $(this).blur();// remove focus input box
			        },
			        duration : 0

		        });			    

			    $("#selectWeek2").datepicker({
			        firstDay : 1, // set first day of week as monday
			        dateFormat: 'dd-mm-yy'
			    });
			    
			    $('#btnPrev').click(function(){

	                var fromDate = $('#selectWeek').datepicker("getDate");
	                setDates(addDays(fromDate, -7));
			    	
			    });
			    
			    $('#btnNext').click(function(){
			    	
	                var fromDate = $('#selectWeek').datepicker("getDate");
	                setDates(addDays(fromDate, 7));

			    });
			    
			    $('#selectWeek').attr('readonly', 'readonly');
			    $('#selectWeek2').attr('readonly', 'readonly');
			    $('#selectWeek').attr('tabindex', '-1');
			    $('#selectWeek2').attr('tabindex', '-1');
			    $('#weeknumber').attr('readonly', 'readonly');
			    
			    setDates(monDate(null));
			    
		    }); // document ready
		    
		    
		</script>
	
	</head>
	<body>

		<%@ include file="/header.jsp" %>
	
		
		<div id="maincontent">
			<div class="innertube">
			
				<input id="btnPrev" type="button" value="Prev"/>
				<input id="selectWeek" style="width: 70px;" />
				<input id="weeknumber" style="width: 20px; "/>
				<input id="selectWeek2" style="width: 70px; " />
				<input id="btnNext" type="button" value="Next"/>
	
				<br />
				<table id="WorkTable"></table>
				<div id="pager1"></div>
				
			</div>
		</div>

	</body>
</html>