<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Timenet</title>
	
		<link rel="shortcut icon" href="/images/timenet.ico" />
	
		<link href="/stylesheets/main.css" rel="stylesheet" type="text/css" />
	
		<link href="/stylesheets/jquery-ui.smoothness.min.css" rel="stylesheet" type="text/css" />
		
		<link href="/stylesheets/ui.jqgrid.css" rel="stylesheet" type="text/css" />

	 	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
     	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
     	 
		<!-- Include jqGrid script file. -->	
		<script src="/script/grid.locale-en.js" type="text/javascript"></script>
		<script src='/script/jquery.jqGrid.min.js' type="text/javascript"></script>
		    
		<script type="text/javascript">
		
		    $(document).ready(function () {
		    	
		    	jQuery.extend(jQuery.jgrid.edit, {
		    	    savekey: [true, 13],
		    	    closeOnEscape: true,
		    	    closeAfterEdit: true,
		    	    closeAfterAdd: true,
		    	    recreateForm: true
		    	});

		        $('#CustomerTable').jqGrid({
		        	url: '/customer',
		        	datatype: 'json',
		        	mtype: 'post',
		        	prmNames: {id: 'customer_id'},
		        	jsonReader: {
		        		root: 'customers',
		        		records: 'totalrecords',
		        		page: 'currentpage',
		        		total: 'totalpages',
		        		repeatitems: false
		        	},
		        	colNames: ['Id', 'Timestamp', 'Name', 'Street', 'Zip', 'City' ],
		        	colModel: [
		        	       { name: 'customer_id', width: 15, key: true, editable: false, hidden:true},
		        	       
		        	       { name: 'timestamp', editable: true, hidden:true},
		        	    	   
		        	       { name: 'customer_name', width: 300, editable: true,
		        	    	   editrules: {required: true}},
		        	    	   
		        	       { name: 'customer_street', width: 200, hidden: true, editable: true, sortable: false,
		        	    	   editrules: {edithidden: true}},
		        	    	   
		        	       { name: 'customer_zipcode', width: 50, hidden:true, editable: true, sortable: false,
		        	    	   editrules: {edithidden: true} },
		        	    	   
		        	       { name: 'customer_city', width: 200, editable: true, sortable: false,
		        	    	   editrules: {required: true} }
		        	    	   
		        	],
		        	pager: '#pager',
		        	rowNum: 10,
		        	sortname: 'customer_name',
		        	viewrecords: true,
		        	caption: 'Customers',
		        	editurl: '/customer',
		        	
		        	ondblClickRow: function(rowid) {
		        		jQuery(this).jqGrid('editGridRow', rowid);
		        	}

		        }).navGrid("#pager", {search: false});
		        
		    });
		</script>
	
	</head>
	<body>
	
		<%@ include file="/header.jsp" %>
	
		
		<div id="maincontent">
			<div class="innertube">
			
				<table id="CustomerTable"></table>
				<div id="pager"></div>
	
			</div>
		</div>
		
	
	</body>
</html>