<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Timenet</title>
	
		<link rel="shortcut icon" href="/images/timenet.ico" />

		<link href="/stylesheets/main.css" rel="stylesheet" type="text/css" />
	
		<link href="/stylesheets/jquery-ui.smoothness.min.css" rel="stylesheet" type="text/css" />
		
		<link href="/stylesheets/ui.jqgrid.css" rel="stylesheet" type="text/css" />

	 	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
     	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
     	 
		<!-- Include jqGrid script file. -->	
		<script src="/script/grid.locale-en.js" type="text/javascript"></script>
		<script src='/script/jquery.jqGrid.min.js' type="text/javascript"></script>
		   
		<script src='/script/timenet.js' type="text/javascript"></script>

		<style type="text/css">
		        div.ui-datepicker { font-size: 11px; }
		</style>
		
		<script type="text/javascript">
		
			
		    $(document).ready(function () {
		    	
		    	jQuery.extend(jQuery.jgrid.edit, {
		    	    savekey: [true, 13],
		    	    closeOnEscape: true,
		    	    closeAfterEdit: true,
		    	    closeAfterAdd: true,
		    	    recreateForm: true
		    	});
		    	
		        $('#ProjectTable').jqGrid({
		        	url: '/project',
		        	datatype: 'json',
		        	mtype: 'post',
		        	recreateForm: true,
		        	ajaxSelectOptions: {type: 'post'},
		        	prmNames: {id: 'project_id'},
		        	jsonReader: {
		        		root: 'projects',
		        		records: 'totalrecords',
		        		page: 'currentpage',
		        		total: 'totalpages',
		        		repeatitems: false
		        	},
		        	colNames: ['Customer', 'Id', 'Timestamp', 'Name', 'Begin', 'End', 'Description' ],
		        	colModel: [
		        	           
		        	       { name: 'customer_id', editable: true, hidden: true,
		        	    	   editrules: {required: true, edithidden: true},
		        	    	   edittype: 'select',  
		        	    	   editoptions: { dataUrl: '/customer?oper=options',
		        	    			   buildSelect:function(response) {
		        	    				   
		        	    				   return buildSelectFromJson(response);

		        	    			   }}
		        	       },
		        	       
		        	       { name: 'project_id', width: 15, key: true, editable: false, hidden:true},
		        	       
		        	       { name: 'timestamp', editable: true, hidden:true},
		        	    	   
		        	       { name: 'project_name', width: 300, editable: true,
		        	    	   editrules: {required: true}},
		        	    	   
		        	       { name: 'project_begin', width: 80, editable: true, sortable: false, "formatter": "date",
		        	    		   "formatoptions": {"srcformat": 'Y-m-d', "newformat": "d-m-Y"},
		        	    		   editoptions: {
		        	    			   dataInit: function(element) {
		        	    				   $(element).datepicker({dateFormat: 'dd-mm-yy'})
		        	    			   }
		        	    		   },
		        	    	   editrules: {required: true}},
		        	    	   
		        	       { name: 'project_end', width: 80,  editable: true, sortable: false, "formatter": "date",
		        	    		   "formatoptions": {"srcformat": 'Y-m-d', "newformat": "d-m-Y"},
		        	    		   editoptions: {
		        	    			   dataInit: function(element) {
		        	    				   $(element).datepicker({dateFormat: 'dd-mm-yy'})
		        	    			   }
		        	    		   }
		        	    	},
		        	    	
		        	    	{ name: 'project_description', hidden: true, width: 300, editable: true, edittype: 'textarea',
			        	    	   editrules: {required: true, edithidden: true}, editoptions: {rows: '3', cols: '20' }
		        	    	}
		        	    	   
		        	],
		        	pager: '#pager1',
		        	rowNum: 10,
		        	sortname: 'project_name',
		        	viewrecords: true,
		        	caption: 'Projects',
		        	editurl: '/project',
		        	
		        	onSelectRow: function(ids) {
		        		if (ids == null) {
		        			
		        		} else {
		        			jQuery("#TaskTable").jqGrid('setGridParam', {url: '/task?project_id=' + ids, page:1});
		        			
		        			var projectGrid = jQuery('#ProjectTable');
		        			var projectrow = projectGrid.jqGrid('getGridParam', 'selrow');
		        			
		        			if (projectrow !== null) {
		        			    // a row of grid is selected and we can get the data from the row
		        			    // which includes the data from all visible and hidden columns
		        			    var projectname = projectGrid.jqGrid('getCell', projectrow, 'project_name');
		        			    
			        			jQuery("#TaskTable").jqGrid('setCaption', 'Tasks of project ' + projectname)
			        				.trigger('reloadGrid');
		        			} 
		        			
		        		}
		        	},
		        	ondblClickRow: function(rowid) {
		        		jQuery(this).jqGrid('editGridRow', rowid);
		        	}

		        }).navGrid("#pager1", {search: false});
				

		        
		        $('#TaskTable').jqGrid({
		        	url: '/task?project_id=0',
		        	datatype: 'json',
		        	mtype: 'post',
		        	recreateForm: true,
		        	ajaxSelectOptions: {type: 'post'},
		        	prmNames: {id: 'task_id'},
		        	width: 500,
		        	jsonReader: {
		        		root: 'tasks',
		        		records: 'totalrecords',
		        		page: 'currentpage',
		        		total: 'totalpages',
		        		repeatitems: false
		        	},
		        	colNames: ['Project', 'Id', 'Timestamp', 'Name', 'Wage', 'Billable', 'Holiday' ],
		        	colModel: [
		        	           
		        	       { name: 'project_id', editable: true, hidden: true,
		        	    	   editrules: {required: true }
		        	       },
		        	       
		        	       { name: 'task_id', width: 15, key: true, editable: false, hidden:true },
		        	       
		        	       { name: 'timestamp', editable: true, hidden:true},
		        	    	   
		        	       { name: 'task_name', width: 300, editable: true,
		        	    	   editrules: {required: true}
		        	       },
		        	       
		        	       { name: 'wage', width: 30, editable: true, hidden: true,
		        	    	   formatter: 'currency', 
		        	    	   editrules: {edithidden: true}
		        	       },
		        	       
		        	       { name: 'billable', width: 40, sortable: false, editable: true, 
		        	    	   edittype: 'checkbox', formatter: 'checkbox', align: 'center',
		        	    	   editoptions: {value: "true:false"} },
		        	       
		        	       { name: 'holiday', width: 40, sortable: false, editable: true, 
		        	    		   edittype: 'checkbox', formatter: 'checkbox', align: 'center',
			        	    	   editoptions: {value: "true:false"}  }
		        	    	   
		        	],
		        	pager: '#pager2',
		        	rowNum: 10,
		        	sortname: 'task_name',
		        	viewrecords: true,
		        	caption: 'Tasks',
		        	editurl: '/task',
		        	
		        	ondblClickRow: function(rowid) {
		        		jQuery(this).jqGrid('editGridRow', rowid);
		        	}

		        }).navGrid("#pager2", {search: false}, 
	        		{// edit options
	        		},
		          { // add options
	        		afterShowForm: function(formid) {
	        			return fillProjectId(formid);
	        		}
		          });
		        
		    });
		    
		    
		    function fillProjectId(formid) {
		    	
    			var projectGrid = jQuery('#ProjectTable');
    			var projectrow = projectGrid.jqGrid('getGridParam', 'selrow');
    			
    			if (projectrow !== null) {
    			    // a row of grid is selected and we can get the data from the row
    			    // which includes the data from all visible and hidden columns
    			    var selRowData = projectGrid.jqGrid('getCell', projectrow, 'project_id');
    			    
    			    $('#project_id', formid).val(selRowData);
    			    
    			    return true;
    			} else {
    				return false;
    			}
		    }
		    
	        
		</script>
	
	</head>
	<body">

		<%@ include file="/header.jsp" %>
	
		
		<div id="maincontent">
			<div class="innertube">
			
				<table id="ProjectTable"></table>
				<div id="pager1"></div>
				<br />
				<br />
				<table id="TaskTable"></table>
				<div id="pager2"></div>
			</div>
		</div>
		
	</body>
</html>