
var oneday = 1000 * 60 * 60 * 24; // millisec

function setDates(monDate) {
	

	if (monDate == null) { monDate = new Date(); }
	
    $("#selectWeek").datepicker("setDate", monDate);

    $("#selectWeek2").datepicker("setDate", addDays(monDate, 6));
    
    var weeknum = $.datepicker.iso8601Week(monDate);
    $('#weeknumber').val(weeknum);

	$('#WorkTable').jqGrid('setGridParam', 
			{postData :{date: $('#selectWeek').val()}}).trigger('reloadGrid');
	
}

function monDate(date) {
	if (date == null) date = new Date();
	
    var weekday = date.getDay();
    
//    console.log("date=" + date);
//    console.log("weekday=" + weekday);
    
    
    if (weekday == 0) { //sunday
    	return addDays(date, -7);
    } else if (weekday != 1) { // monday
    	return addDays(date, 1 - weekday);
    }
    return date;
}

function firstDayOfMonth(date) {
	if (date == null) date = new Date();
	
	var date2 = new Date(date.getFullYear(), date.getMonth(), 1);
    
    return date2;
}

function lastDayOfMonth(date) {
	if (date == null) date = new Date();
	
	var month = date.getMonth();
	var year = date.getFullYear();
	
	if (month == 12) {
		year += 1;
		month = 1;
	} else {
		month += 1;
	}
	
	var date2 = addDays(new Date(year, month, 1), -1);
    
    return date2;
}

function addDays(fromDate, days) {
	if (fromDate == null) fromDate = new Date();
	
    var mytime = fromDate.getTime() / oneday;
    
    mytime = mytime + days;

    return new Date(mytime * oneday);
}

function addMonth(fromDate, months) {
	if (fromDate == null) fromDate = new Date();
	
	var month = fromDate.getMonth();
	var year = fromDate.getFullYear();
	
	
	if (months > 0) {
		
		if (month + months > 12) {
			year += 1;
			month = month + months - 12;
		} else {
			month += months;
		}
		
	} else if (months < 0) {
		
		if (month - months < 1) {
			year -= 1;
			month = month - months + 12;
		} else {
			month += months;
		}
		
	}
	
    return new Date(year, month, 1);
}


function buildSelectFromJson(data) {
	
//	console.log('buildSelectFromJson');
		
	var sel = "<select>";
	

	var obj = $.parseJSON(data);
    $.each(obj, function(k, v) {
//          console.log(k + " : "+ v);
          sel += '<option value="' + k + '">' + v + '</option>';
	});

	sel += "</select>";
	
//	console.log(sel);
	return sel;

}
