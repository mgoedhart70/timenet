<!DOCTYPE html>
<%@ page import="java.util.*" %>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Timenet</title>
		
		<link rel="shortcut icon" href="/images/timenet.ico" />

		<link href="/stylesheets/main.css" rel="stylesheet" type="text/css" />
	
		<link href="/stylesheets/jquery-ui.smoothness.min.css" rel="stylesheet" type="text/css" />
	
	 	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	   	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
	     	
		<script src='/script/timenet.js' type="text/javascript"></script>
		<!-- http://josscrowcroft.github.io/accounting.js/ -->
		<script src='/script/accounting.min.js' type="text/javascript"></script>
		
		<style type="text/css">
		    div.ui-datepicker { font-size: 11px; }
		</style>
		
		<script type="text/javascript">
		
			$(document).ready(function () {
				
				accounting.settings.currency.symbol = "€";
				accounting.settings.currency.precision = 2;
				accounting.settings.currency.thousand = ".";
				accounting.settings.currency.decimal = ",";
				
				accounting.settings.number.precision = 1;
				accounting.settings.number.decimal = ",";
				
			    $("#startDate").datepicker({
			    	
			        firstDay : 1, // set first day of week as monday
			        changeMonth : true, // provide option to select Month
			        changeYear : true, // provide option to select year
			        showWeek: true,
			        dateFormat: 'dd-mm-yy',
			        showButtonPanel : true // button panel having today and done button
	
		        });			    
	
			    $("#endDate").datepicker({
			        firstDay : 1, // set first day of week as monday
			        dateFormat: 'dd-mm-yy',
			        changeMonth : true, // provide option to select Month
			        changeYear : true, // provide option to select year
			        showWeek: true
			    });
	
			    $("#startDate").datepicker("setDate", firstDayOfMonth(null));
			    $("#endDate").datepicker("setDate", lastDayOfMonth(null));
	
			    $('#btnReport').click(function() {
			    	
			    	var startDate = $("#startDate").datepicker({dateFormat: 'dd-mm-yy'}).val();
			    	var endDate = $("#endDate").datepicker({dateFormat: 'dd-mm-yy'}).val();
			    	
			    	var jqxhr = $.post("/work?oper=report", {startdate: startDate, enddate: endDate, 
			    		                                     customer_id: $("#customer_id").val()});
			    	
			    	jqxhr.done(function(data) { 
	
			    		$('#report').empty();
	
			    		$('#report').append("<br /><table>");
			    		
			    		var customername;
			    		var projectname;
			    		var totalhours = 0;
			    		var totalwage = 0;
			    		
			    		for (var i = 0; i < data.reports.length;i++) {
	
				    		$('#report').append("<tr>");
			    			
				    		if (customername != data.reports[i].customer_name) {
					    		$('#report').append("<td class='report name'>" + data.reports[i].customer_name + "</td>");
				    			customername = data.reports[i].customer_name;
			    			} else {
					    		$('#report').append("<td class='report name'></td>");
			    			}
			    			
				    		if (projectname != data.reports[i].project_name) {
					    		$('#report').append("<td class='report name'>" + data.reports[i].project_name + "</td>");
					    		projectname = data.reports[i].project_name;
				    		} else {
					    		$('#report').append("<td class='report name'></td>");
				    		}
				    		
				    		$('#report').append("<td class='report name'>" + data.reports[i].task_name + "</td>");
				    		$('#report').append("<td class='report number'>" + 
				    				accounting.formatNumber(data.reports[i].hours) + "</td>");
				    		
				    		$('#report').append("<td class='report number'>" + 
				    				accounting.formatMoney(data.reports[i].wage) + "</td>");
				    		
				    		var taskwage = (data.reports[i].hours * data.reports[i].wage); 
				    		$('#report').append("<td class='report number'>" + 
				    				accounting.formatMoney(taskwage) + "</td>");
				    		
				    		$('#report').append("</tr>");
				    		
				    		totalhours += data.reports[i].hours;
				    		totalwage += taskwage;
				    		
			    		}
			    		$('#report').append("<tr>");
			    		$('#report').append("<td colspan='3'></td><td colspan='3' class='report number'><hr /></td>");
			    		$('#report').append("</tr>");
			    		$('#report').append("</table>");
			    		
			    		$('#report').append("<tr>");
			    		$('#report').append("<td colspan='3'></td><td class='report number'>" + 
			    				accounting.formatNumber(totalhours) + "</td>");
			    		$('#report').append("<td colspan='1'></td><td class='report number'>" + 
			    				accounting.formatMoney(totalwage) + "</td>");
			    		$('#report').append("</tr>");
			    		$('#report').append("</table>");
			    		
			    	});
			    	
			    	jqxhr.fail(function() { 
			    		$('#report').empty().append("error");
			    	});
			    	
			    	jqxhr.always(function(data) { 
			    		// $('#report').append(" - always");
			    	});

			    });
			    
			    var jqxhr = $.ajax({
			    	url: '/customer?oper=options',
			    	dataType: 'json',
			    	type: 'post'
			    });
			    
			    jqxhr.done(function(data) {
			    	
			    	var sel = '<select id="customer_id"><option value="0">-</option>';
			    	
			        $.each(data, function(key, val) {
//			              console.log(key + " : "+ val);
			              sel += '<option value="' + key + '">' + val + '</option>';
			    	});

			        sel += "</select>";
			    	$('#customer_select').append(sel);
			    	
			    });

			    $('#btnPrev').click(function(){

	                var fromDate = $('#startDate').datepicker("getDate");
	                
	                var newDate = addMonth(fromDate, -1);
	                
	                $("#startDate").datepicker("setDate", newDate);
	                $("#endDate").datepicker("setDate", lastDayOfMonth(newDate));

			    	
			    });

			    $('#btnNext').click(function(){

	                var fromDate = $('#startDate').datepicker("getDate");
	                
	                var newDate = addMonth(fromDate, 1);
	                
	                $("#startDate").datepicker("setDate", newDate);
	                $("#endDate").datepicker("setDate", lastDayOfMonth(newDate));
			    	
			    });
			    
		    }); // document ready
		    
		</script>
		
	</head>
	<body>
	
		<%@ include file="/header.jsp" %>
	
		<div id="maincontent">
		
			<div class="innertube">
				
				<table>
					<tr>
					<td><input id="btnPrev" type="button" value="Prev"/></td>
					<td><input id="startDate" style="width: 70px;" /> - <input id="endDate" style="width: 70px; " /></td>
					<td><input id="btnNext" type="button" value="Next"/></td>
					<td><input id="btnReport" type="button" value="Report"/></td>
					<td><div id="customer_select"></div></td>
					</tr>
				</table>
				
				<br />
				
				<div id="report"><br />
				
				</div>
				
			</div>
			
			
		</div>
	
	</body>
</html>